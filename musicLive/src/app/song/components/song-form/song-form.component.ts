import { Component, OnInit } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { UploadService } from 'src/app/shared/service/upload.service';
import { MatSnackBar } from '@angular/material';
import { requiredFileType } from 'src/app/shared/validators/requiredFileType';
import { SongService } from '../../services/song.service';
import { ISize } from 'selenium-webdriver';
import { ISong } from 'src/app/shared/interfaces/song.model';

@Component({
  selector: 'app-song-form',
  templateUrl: './song-form.component.html',
  styleUrls: ['./song-form.component.css']
})
export class SongFormComponent implements OnInit {

  songForm: FormGroup;
  album:IAlbum;
  error:any;
  constructor(private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private uploadService: UploadService,
    public snackBar: MatSnackBar,
    private songService:SongService){
      
  }

  ngOnInit() {
    this.album = this.route.snapshot.data['album'];
    this.songForm = this.fb.group({
      name: ['toni', [Validators.required]],
      image: [null, [Validators.required, requiredFileType('mp3')]],
    });
  }

  get f() { return this.songForm.controls; }

  submit() {
    if (this.songForm.valid) {
      let params:any = {
        name:this.f.name.value,
        album:this.album,

      }

      this.songService.addSong(<ISong>params).subscribe((resp) => {
        if (resp) {
          console.log(resp);
          this.uploadService.uploadSong(resp._id, this.songForm.get('image').value).subscribe((resp) => {
            this.snackBar.open("Album uploaded", "close", { panelClass: ['green-snackbar'] });
            this.router.navigateByUrl(`/album/${this.album._id}`);
          }, error => this.error = error)
        }
        else{
          console.log(resp);
        }
      },
        error => {
          this.error = error;
        });
    }
  }

}
