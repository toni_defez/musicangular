import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SongRoutingModule } from './song-routing.module';
import { SongFormComponent } from './components/song-form/song-form.component';
import { SharedModule } from '../shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SongFormComponent],
  imports: [
    CommonModule,
    SongRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports:[
    SongFormComponent
  ]
})
export class SongModule { }
