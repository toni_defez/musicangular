import { Injectable } from '@angular/core';
 
import { Observable } from 'rxjs';

import { map } from 'rxjs/operators';

import { ISong } from 'src/app/shared/interfaces/song.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SongService {

  constructor(private http: HttpClient) { }

  getSongByAlbum(id:string):Observable<ISong[]>{
    return this.http.get<{ songs:ISong[] }>(`song/album/${id}`).pipe(
      map(resp => {
        const r = resp.songs;
        return r;
      })
    );
  }

  getSongById(id:string):Observable<ISong>{
    return this.http.get<{ song:ISong }>(`song/${id}`).pipe(
      map(resp => {
        const r = resp.song;
        r.file =  `${environment.baseUrl}/song/get-file-song/${r.file}`;
        r.album.image = `${environment.baseUrl}/album/get-image-album/${r.album.image}`;
        return r;
      })
    );
  }

  addSong(song: ISong): Observable<ISong> {
    return this.http.post<{ song: ISong }>(`song/save`, song).pipe(
      map(resp => {
        console.log(resp);
        const r = resp.song;
        return r;
      })
    );
  }

  deleteSong(song:string): Observable<void> {
    console.log(song);
    return this.http.delete<void>(`song/${song}`);
  }





  //servicios para redux
  getConcreteInformation(song:ISong):Observable<any>{
    console.log(song);
    return this.http.get(`song/${song._id}`);
  }

}
