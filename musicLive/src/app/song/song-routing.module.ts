import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SongFormComponent } from './components/song-form/song-form.component';
import { AlbumResolve } from '../album/resolve/album-resolve.service';
import { AdminActivateGuard } from '../guards/admin-activate.guard';

const routes: Routes = [
  {
    path: 'add/:id',
    component:SongFormComponent,
    pathMatch:'full',
    canActivate: [AdminActivateGuard],
    resolve:{
      album:AlbumResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SongRoutingModule { }
