import { Action } from '@ngrx/store';
import { ISong } from 'src/app/shared/interfaces/song.model';


export enum PlayListActionTypes {
    SelectedPlaylist='[PlayList] PlayList seleccionado',
    SelectedSong = '[PlayList] Cancion seleccionado',
    SuccesfulSelectedSong = '[PlayList] Playlist Actualizado con cancion',
    RemoveSong= 'PLAYER_REMOVE_SONG',
    RemoveAllSong='PLAYER_REMOVE_ALL_SONGS',
    SelectedPlaylistPersonal = '[PlayList] Personal Selected Playlist',
    SuccesfulSelectedPlaylistPersonal = '[PlayList] Succesful Selected Playlist',
    AddedSongToQueue = '[Playlist] Added song to queue',
    SelectedArtistPlaylist='[Playlist] Artist playlist'


    
}


export class SelectedPlayList implements Action {
    readonly type = PlayListActionTypes.SelectedPlaylist;
    constructor(public payload?: any) { }
}
//Seleccionar la playlist
export class SelectedPlaylistPersonal implements Action {
    readonly type = PlayListActionTypes.SelectedPlaylistPersonal;
    constructor(public payload?:any){}
}

export class SuccesfulSelectedPlaylistPersonal implements Action {
    type:string = PlayListActionTypes.SuccesfulSelectedPlaylistPersonal;
    constructor(public payload:any){}
}

export class SelectedSong implements Action {
    readonly type = PlayListActionTypes.SelectedSong;
    constructor(public payload?:any){};
}

export class SuccesfulSelectedSong implements Action{
    readonly type: PlayListActionTypes.SuccesfulSelectedSong;
    constructor(public payload?:any){};
}

export class RemoveSong implements Action {
    type: string = PlayListActionTypes.RemoveSong;
    constructor(public payload: any) { }
}

export class RemoveAllSongs implements Action {
    type: string = PlayListActionTypes.RemoveAllSong;
    constructor(public payload: any) { }
}

//Añadimos cancion a la cola de rep
export class AddedSongToQueue implements Action{
    readonly type = PlayListActionTypes.AddedSongToQueue;
    constructor(public payload?:ISong){}
}

//action para añadir todas las canciones del artista

export class SelectedArtistPlaylist implements Action{
    readonly type = PlayListActionTypes.SelectedArtistPlaylist;
    constructor(public payload?:any){};
}







export type All = SelectedPlayList | SelectedSong | 
                    SuccesfulSelectedSong |RemoveSong | RemoveAllSongs
                    | SelectedPlaylistPersonal | 
                    SuccesfulSelectedPlaylistPersonal  | AddedSongToQueue
                    |SelectedArtistPlaylist;
  
