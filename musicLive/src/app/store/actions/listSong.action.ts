import { Action } from '@ngrx/store';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';

export enum ListSongActionTypes {
    LoadMePlaylist = '[ListSong] Loaded my Playlist',
    SuccesfulMePlaylist = '[ListSong] Succesful loaded playlist',
    PLAYLIST_ADD_SONG= 'PLAYLIST_ADD_SONG',
    PLAYLIST_ADDED_SONG= 'PLAYLIST_ADDED_SONG',
    PLAYLIST_REMOVE= 'PLAYLIST_REMOVE',
    PLAYLIST_ADD_NEW = '[ListSong] New playlist created!!'
}

export class LoadMePlaylist implements Action{
    readonly type = ListSongActionTypes.LoadMePlaylist;
    constructor(public payload ?:any){}
}



export class SuccesfulMePlaylist implements Action{
    readonly type = ListSongActionTypes.SuccesfulMePlaylist;
    constructor(public payload ?:any){}
}

export class PlaylistAddNew implements Action{
    readonly type = ListSongActionTypes.PLAYLIST_ADD_NEW;
    constructor(public payload?:IPlaylist){}
}

export class PlaylistRemovedAction implements Action {
    type: string = ListSongActionTypes.PLAYLIST_REMOVE;
    constructor(public payload?:any) {}
}


//NOT IMPLEMENTED YET
export class PlaylistAddedSongAction implements Action {
    type: string = ListSongActionTypes.PLAYLIST_ADDED_SONG;
    constructor(public payload: any) { }
}
export class PlaylistAddSongAction implements Action {
    type: string = ListSongActionTypes.PLAYLIST_ADD_SONG;
    constructor(public album:IAlbum,public id:string) { }
}

export type All =
     LoadMePlaylist| 
     SuccesfulMePlaylist|
     PlaylistAddedSongAction|
     PlaylistAddSongAction|
     PlaylistRemovedAction;
