import { Action } from '@ngrx/store';


export enum AlbumActionTypes {
    LOAD = '[ALBUM] Loaded albums',
    SucessfulLoadAlbums = '[ALBUM] EXITO albums',
    SelectedAlbum='[Album] Album seleccionado'
}

export class LoadAlbums implements Action {
    readonly type = AlbumActionTypes.LOAD;
    constructor(public payload?: any) { }
}

export class SucessfulLoadAlbums implements Action {
    readonly type = AlbumActionTypes.SucessfulLoadAlbums;
    constructor(public payload?: any) { }
}

export class SelectedAlbum implements Action{
    readonly type = AlbumActionTypes.SelectedAlbum;
    constructor(public payload?: any) { }
}
  
export type All =LoadAlbums | SucessfulLoadAlbums | SelectedAlbum;