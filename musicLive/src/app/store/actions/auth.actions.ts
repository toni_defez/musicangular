import { Action } from '@ngrx/store';



export enum AuthActionTypes {
  LOGIN = '[Auth] Login',
  LOGIN_SUCCESS = '[Auth] Login Success',
  LOGIN_FAILURE = '[Auth] Login Failure',
  SIGNUP = '[Auth] Signup',
  SIGNUP_SUCCESS = '[Auth] Signup Success',
  SIGNUP_FAILURE = '[Auth] Signup Failure',
  LOGOUT = '[Auth] Logout',
  GET_STATUS = '[Auth] GetStatus',
  USER_LOAD = '[Auth] UserLoaded',
  SUCCESFUL_USER_LOAD = '[Auth] Succesful user load'
}

export class LogIn implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload?: any) { }

}

export class LogInSuccess implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: any) { }
}

export class LogInFailure implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: any) { }
}

export class Signup implements Action {
  readonly type = AuthActionTypes.SIGNUP;
  constructor(public payload?: any) { }
}

export class SignupSucces implements Action{
  readonly type = AuthActionTypes.SIGNUP_SUCCESS;
  constructor(public payload?:any){}
}

export class SignupFailure implements Action{
  readonly type = AuthActionTypes.SIGNUP_FAILURE;
  constructor(public payload?:any){}
}

export class LogOut implements Action {
  readonly type = AuthActionTypes.LOGOUT;
}

export class UserLoad implements Action{
  readonly type = AuthActionTypes.USER_LOAD;
  constructor(public payload?:any){}
}

export class SuccesfulUserLoad implements Action{
  readonly type = AuthActionTypes.SUCCESFUL_USER_LOAD;
  constructor(public payload?:any){}
}

export type All =
  | LogIn | LogInSuccess | LogInFailure | Signup |
  SignupFailure|SignupSucces|LogOut|UserLoad|SuccesfulUserLoad ;