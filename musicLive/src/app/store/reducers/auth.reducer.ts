import { IUser } from 'src/app/shared/interfaces/user.model';
import { AuthActionTypes, All } from '../actions/auth.actions';



export interface State {
  isAuthenticated: boolean;
  user: any | null;
  errorMessage: string | null;
  client:any|null;
}

export const initialState: State = {
    isAuthenticated: false,
    user: null,
    errorMessage: null,
    client:null
};

export function AuthReducer(state = initialState, action: All): State {
    switch (action.type) {
      case AuthActionTypes.LOGIN_SUCCESS: {
        return {
          ...state,
          isAuthenticated: true,
          user: {
            token: action.payload.token,
            email: action.payload.email
          },
          errorMessage: null
        };
      }
      
      case AuthActionTypes.LOGIN_FAILURE: {
        return {
          ...state,
          errorMessage: 'Incorrect email and/or password.'
        };
      }

      case AuthActionTypes.SIGNUP_SUCCESS: {
        return {
          ...state,
          isAuthenticated: true,
          user: {
            token: action.payload.token,
            email: action.payload.user.email
          },
          errorMessage: null
        };
      }

      case AuthActionTypes.SIGNUP_FAILURE: {
        return {
          ...state,
          errorMessage: 'Incorrect values for register.'
        };
      }

      case AuthActionTypes.SUCCESFUL_USER_LOAD:{

        localStorage.setItem('user', JSON.stringify(action.payload.user));
        return {
          ...state,
         client:action.payload.user
        };
      }

      case AuthActionTypes.LOGOUT: {
        return initialState;
      }
      
      default: {
        return state;
      }
    }
  }