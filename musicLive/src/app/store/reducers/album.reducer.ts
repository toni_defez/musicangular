import { All, AlbumActionTypes } from '../actions/album.actions';


export interface State {
    album: any | null;
    errorMessageAlbum: string | null;
    albumSelected:any|null;
  }

  export const initialState: State = {
    album: null,
    errorMessageAlbum: null,
    albumSelected:null
};

export function AlbumReducer(state = initialState, action: All): State {
    switch (action.type) {
      
        case AlbumActionTypes.LOAD: {
            return {
              ...state,
              album:action.payload,
              errorMessageAlbum: null
            };
          }
       
        case AlbumActionTypes.SucessfulLoadAlbums:{
          return {
            ...state,
            album:action.payload.albums,
            errorMessageAlbum: null
          };
        }

        case AlbumActionTypes.SelectedAlbum:{
          return {
            ...state,
            albumSelected:action.payload,
            errorMessageAlbum: null
          };
        }

         case AlbumActionTypes.SelectedAlbum:{
          return {
            ...state,
            albumSelected:action.payload,
            errorMessageAlbum: null
          };
        }

        default: {
          return state;
        }
    }
}