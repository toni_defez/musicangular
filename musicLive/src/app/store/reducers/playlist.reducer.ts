import { All, PlayListActionTypes } from '../actions/playlist.actions';
import { ISong } from 'src/app/shared/interfaces/song.model';
import { environment } from 'src/environments/environment';

export interface State {
    songs: any | null;
    errorMessageSong: string | null;
}

export const initialState: State = {
    songs: [],
    errorMessageSong: null,
};


export function PlayListReducer(state = initialState, action: All): State {
    switch (action.type) {

        case PlayListActionTypes.AddedSongToQueue:{

            let newSong:ISong = action.payload;
            newSong.album.image = `${environment.baseUrl}/album/get-image-album/${newSong.album.image}`;
            return { 
                ...state,
                songs: [...state.songs, newSong]
            }
        }

        case PlayListActionTypes.SelectedPlaylist: {
            return {
                ...state,
                songs: action.payload.songs,
                errorMessageSong: null
              };
        }

        case PlayListActionTypes.SelectedSong:{
            return {
                ...state,
                songs:[action.payload],
                errorMessageSong:null
            }
        }

        case PlayListActionTypes.SuccesfulSelectedSong:{
            return {
                ...state,
                songs:[action.payload.song],
                errorMessageSong:null
            }
        }
        
        case PlayListActionTypes.RemoveSong:
            const songsUpdated = state.songs.filter(song => {
                return song != action.payload.song;
            });
            return Object.assign(state, { songs: songsUpdated });
        
        case PlayListActionTypes.RemoveAllSong:
            const songsCleared = [];
            return Object.assign(state, { songs: songsCleared });

        case PlayListActionTypes.SuccesfulSelectedPlaylistPersonal:
                return {
                    ...state,
                    songs:action.payload.playlist.songs,
                    errorMessageSong:null
                }
        
        case PlayListActionTypes.SelectedArtistPlaylist:
            return {
                ...state,
                songs:action.payload
            }

        default: {
            return state;
          }
    }
}