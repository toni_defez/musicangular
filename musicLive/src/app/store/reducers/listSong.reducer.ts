import { All, PlayListActionTypes } from '../actions/playlist.actions';
import { ListSongActionTypes } from '../actions/listSong.action';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { environment } from 'src/environments/environment';

export interface State {
    MEplaylist:any|null;
    errorMessageSong: string | null;
}

export const initialState: State = {
    MEplaylist:[],
    errorMessageSong: null,
};


export function ListSongReducer(state = initialState, action: All): State {
    switch (action.type) {

        case ListSongActionTypes.SuccesfulMePlaylist:
            let playlistArray:IPlaylist[] = action.payload.playlist;
            playlistArray = playlistArray.map(item=>{
                item.file =  `${environment.baseUrl}list/get-image-playlist/${item.file}`;
                item.userCreator.image =  `${environment.baseUrl}user/get-image-user/${item.userCreator.image}`;
                return item;
            })
            console.log(playlistArray);
            return {
                ...state,
                MEplaylist:playlistArray
            }

        //CREACION DE NUEVA Playlist
        case ListSongActionTypes.PLAYLIST_ADD_NEW:

                let playlistNew:IPlaylist = action.payload;
                playlistNew.file =  `${environment.baseUrl}list/get-image-playlist/${playlistNew.file}`;
                playlistNew.userCreator.image =  `${environment.baseUrl}user/get-image-user/${playlistNew.userCreator.image}`;
                return { 
                    ...state,
                    MEplaylist: [...state.MEplaylist, playlistNew]
                }

        case ListSongActionTypes.PLAYLIST_REMOVE:
            
                const songsRemoved = state.MEplaylist.filter(song => {
                    return song != action.payload;
                });
                return Object.assign(state, { MEplaylist: songsRemoved });

        default: {
            return state;
          }
    }

}