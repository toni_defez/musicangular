import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, mergeMap } from 'rxjs/operators';
import { SongService } from 'src/app/song/services/song.service';
import { PlayListActionTypes, SelectedSong, SuccesfulSelectedSong, SelectedPlaylistPersonal } from '../actions/playlist.actions';
import { PlaylistService } from 'src/app/playlist/services/playlist.service';



@Injectable()
export class PlayListEffects {
    constructor(
        private actions: Actions,
        private songService: SongService,
        private router: Router,
        private playlistService:PlaylistService
      ) { }

    //Esto hace referencia al conjunto de canciones de un album 
    @Effect( )
    SelectedSong: Observable<any> = this.actions.pipe(
        ofType(PlayListActionTypes.SelectedSong),
        mergeMap((action: any) => this.songService.getConcreteInformation(action.payload).pipe(
            map(response=> //new SuccesfulSelectedSong(response))
                (( {type: PlayListActionTypes.SuccesfulSelectedSong, payload: response })))
                ,
            catchError(() => "eRROR")
        ))
    )

    //Esto hace referencia de un conjunto de canciones de una Playlist
    @Effect()
    SelectedPlaylistPersonal:Observable<any> = this.actions.pipe(
        ofType(PlayListActionTypes.SelectedPlaylistPersonal),
        mergeMap((action:any)=> this.playlistService.getPlayListRedux(action.payload._id).pipe(
            map(response=>
                (( {type: PlayListActionTypes.SuccesfulSelectedPlaylistPersonal, payload: response })))
                ,
            catchError(() => "eRROR"))
        ))

}