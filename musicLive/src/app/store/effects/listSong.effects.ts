import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Observable, of } from 'rxjs';
import { catchError, map, switchMap, mergeMap } from 'rxjs/operators';
import { SongService } from 'src/app/song/services/song.service';
import { PlayListActionTypes, SelectedSong, SuccesfulSelectedSong, SelectedPlaylistPersonal } from '../actions/playlist.actions';
import { PlaylistService } from 'src/app/playlist/services/playlist.service';
import { ListSongActionTypes } from '../actions/listSong.action';


@Injectable()
export class ListSongEffects {
    constructor(
        private actions: Actions,
        private songService: SongService,
        private router: Router,
        private playlistService:PlaylistService
      ) { }

      @Effect()
      LoadMePlaylist:Observable<any> = this.actions.pipe(
          ofType(ListSongActionTypes.LoadMePlaylist),
          mergeMap((action:any)=>this.playlistService.getMePlaylistRedux(action.payload._id).pipe(
              map(response=>
                  (( {type: ListSongActionTypes.SuccesfulMePlaylist, payload: response }))
                  ,
                  catchError(()=>"error"))
          ))
      )
      
    }