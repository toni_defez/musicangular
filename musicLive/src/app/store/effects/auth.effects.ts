import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';



import { Observable, of, pipe } from 'rxjs';
import { catchError, map, switchMap, mergeMap, windowCount } from 'rxjs/operators';
import { tap } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/services/auth.service';
import { AuthActionTypes, LogIn, LogInSuccess, LogInFailure, Signup, SignupSucces, SignupFailure, SuccesfulUserLoad } from '../actions/auth.actions';
import { UserService } from 'src/app/user/services/user.service';
import { UploadService } from 'src/app/shared/service/upload.service';
import { ListSongActionTypes } from '../actions/listSong.action';

@Injectable()
export class AuthEffects {

  constructor(
    private actions: Actions,
    private authService: AuthService,
    private userService:UserService,
    private router: Router,
    private uploadService:UploadService
  ) { }

  @Effect()
  LogIn: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN),
    mergeMap((action: any) => this.authService.logIn(action.payload).pipe(
      map(movies => new LogInSuccess({
        token: movies.token,
        email: action.payload.email
      })
      ),
      catchError((error) => {
        return of(new LogInFailure({ error: error }));
      }
      )
    ))
  )

  @Effect()
  LoadUser:Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.USER_LOAD),
    mergeMap((action:any)=>this.userService.getUserRedux().pipe(
      mergeMap(response=>[({ type: AuthActionTypes.SUCCESFUL_USER_LOAD,
         payload: response }),
         ({
          type: ListSongActionTypes.LoadMePlaylist,
          payload: response.user
         })
        ])
      
    ))
  )


  
  @Effect({ dispatch: false })
  LogInSuccess: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_SUCCESS),
    tap((user) => {
      localStorage.setItem('token', user.payload.token);
      this.router.navigateByUrl('/home');
    })
  );

  @Effect({ dispatch: false })
  LogInFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.LOGIN_FAILURE)
  );

  /**
   * name:this.form.value.name,
        surname:this.form.value.surname,
        email:this.form.value.email,
        password:
   */
  @Effect()
  Signup: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SIGNUP),
    mergeMap((action: any) => this.authService.signUp({
      "name":action.payload.name,
      "surname":action.payload.surname,
      "email":action.payload.email,
      "password":action.payload.password,
    }).pipe(
      map(response =>{
        this.router.navigateByUrl('/home');
        localStorage.setItem('token', response.token);
        return new SignupSucces({
          user:response.user,
          token:response.token,
          image:action.payload.image
        })
      } 
      ),
      catchError((error) => {
        console.log(error);
        return of(new LogInFailure({ error: error }));
      }
      )
    ))
  )

  @Effect()
  SignUpSuccess: Observable<any> = this.actions.pipe(
  ofType(AuthActionTypes.SIGNUP_SUCCESS),
  mergeMap((action:any) => 
      this.uploadService.upload(action.payload.user.id,action.payload.image)
  .pipe(
    map(movies => ({ type: '[Movies API] Movies Loaded Success', payload: movies })),
    catchError(() => of({ type: '[Movies API] Movies Loaded Error' }))
  ))
)
  //tap((response) => {
   // console.log("hola");
   // console.log(response);
    //subimos la foto al servidor
    //this.uploadService.upload(user.payload.)
    //localStorage.setItem('token', response.payload.token);
    //this.router.navigateByUrl('/');
 // })
  

  @Effect({ dispatch: false })
  SignupFailure: Observable<any> = this.actions.pipe(
    ofType(AuthActionTypes.SIGNUP_FAILURE)
  );

  @Effect({ dispatch: false })
  public LogOut: Observable<any> = this.actions.pipe(
  ofType(AuthActionTypes.LOGOUT),
  tap((user) => {
    localStorage.removeItem('token');
    this.router.navigateByUrl('/auth/login');
  })
  );



}