import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Router } from '@angular/router';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AlbumService } from 'src/app/album/services/album.service';
import { Observable, of } from 'rxjs';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';
import { AlbumActionTypes, SelectedAlbum } from '../actions/album.actions';
import { PlayListActionTypes, SelectedSong, SelectedPlayList } from '../actions/playlist.actions';
import { PlaylistService } from 'src/app/playlist/services/playlist.service';





@Injectable()
export class AlbumEffects {
    constructor(
        private actions: Actions,
        private albumService: AlbumService,
        private router: Router,
        private playlistService:PlaylistService
      ) { }

      @Effect()
      LoadAlbums: Observable<any> = this.actions.pipe(
         ofType(AlbumActionTypes.LOAD),
         mergeMap((action: any) => this.albumService.getAlbumsRedux().pipe(
            map(response => 
              (
                { type: AlbumActionTypes.SucessfulLoadAlbums, payload: response }
                )
              ),
            catchError(() => "eRROR")
        ))
      )

      @Effect()
      SelectedAlbum:Observable<any> = this.actions.pipe(
        ofType(AlbumActionTypes.SelectedAlbum),
        mergeMap((action:SelectedAlbum)=>this.albumService.getSongAlbumRedux(action.payload._id).pipe(
          map(response=>(( {type: PlayListActionTypes.SelectedPlaylist, payload: response })))
        ))
      )

}