import * as auth from './reducers/auth.reducer';
import * as album from './reducers/album.reducer';
import * as playlist from './reducers/playlist.reducer';
import * as song from './reducers/listSong.reducer';
import { createFeatureSelector } from '@ngrx/store';
import { AuthEffects } from './effects/auth.effects';
import { AlbumEffects } from './effects/album.effects';
import { PlayListEffects } from './effects/playlist.effects';
import { ListSongEffects } from './effects/listSong.effects';


export interface AppState {
    authState: auth.State;
    albumState:album.State;
    playListState:playlist.State;
    listSong:song.State
}

export const reducers = {
  auth: auth.AuthReducer,
  album:album.AlbumReducer,
  playlist:playlist.PlayListReducer,
  listSong:song.ListSongReducer
};

export const selectAuthState = createFeatureSelector<AppState>('auth');
export const selectAlbumState = createFeatureSelector<AppState>('album');
export const selectedPlayListState = createFeatureSelector<AppState>('playlist');
export const selectedListSong = createFeatureSelector<AppState>('listSong');
export const effects = [
  AuthEffects,
  AlbumEffects,
  PlayListEffects,
  ListSongEffects
]