import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { ISong } from 'src/app/shared/interfaces/song.model';

@Injectable({
  providedIn: 'root'
})
export class AlbumService {

  constructor(private http: HttpClient) { }

  getAlbums(): Observable<IAlbum[]>{
    return this.http.get<{albums:IAlbum[]}>('album/getAll').pipe(
      map(resp =>
        resp.albums.map(r => {
          r.image = `${environment.baseUrl}/album/get-image-album/${r.image}`;
          return r;
        }),
        catchError((resp: HttpErrorResponse) => throwError(`Error register
        user. Status: ${resp.status}. Message: ${resp.message}`))
      )
    );
  }

  deleteAlbum(id: string): Observable<void> {
    return this.http.delete<void>(`album/${id}`);
  }

  getSongAlbum(id:string):Observable<ISong[]>{

    return this.http.get<{ songs:ISong[] }>(`song/album/${id}`).pipe(
      map(resp => {
        let array:ISong[] = resp.songs.map(item=>{
          item.file =  `${environment.baseUrl}/song/get-file-song/${item.file}`;
          item.album.image = `${environment.baseUrl}/album/get-image-album/${item.album.image}`;
          return item;
        });
        return array;
      })
    );
  }

  getAlbum(id:string):Observable<IAlbum>{
    return this.http.get<{ album:IAlbum }>(`album/${id}`).pipe(
      map(resp => {
        const r = resp.album;
        r.image = `${environment.baseUrl}/album/get-image-album/${r.image}`;
        return r;
      })
    );
  }

  getAlbumByArtist(id:string):Observable<IAlbum[]>{
    return this.http.get<{albums:IAlbum[]}>(`album/artist/${id}`).pipe(
      map(resp=>{
        const r = resp.albums;
        console.log(resp.albums);
        r.forEach((item:any)=>{

          item.image = `${environment.baseUrl}/album/get-image-album/${item.album.image}`;
          item.songsOfAlbum.forEach(song=>{
            song.file =  `${environment.baseUrl}/song/get-file-song/${song.file}`;
          })
        });
        return r;
      })
    )
  }

  addAlbum(album: IAlbum): Observable<IAlbum> {
    return this.http.post<{ album: IAlbum }>(`album/save`, album).pipe(
      map(resp => {
        const r = resp.album;
        return r;
      })
    );
  }

  

  //METODOS PARA REDUX
  getAlbumsRedux(): Observable<any>{
    return this.http.get('album/getAll');
  }

  getSongAlbumRedux(id:string):Observable<any>{
    console.log("idddd",id);
    return this.http.get<{ songs:ISong[] }>(`song/album/${id}`);
  }
}
