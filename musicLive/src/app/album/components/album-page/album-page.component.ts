import { Component, OnInit } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { AlbumService } from '../../services/album.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Store } from '@ngrx/store';
import { AppState, selectAlbumState } from 'src/app/store/app.state';
import { Observable, Subscription } from 'rxjs';
import { LoadAlbums } from 'src/app/store/actions/album.actions';
import { state } from '@angular/animations';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-album-page',
  templateUrl: './album-page.component.html',
  styleUrls: ['./album-page.component.scss']
})
export class AlbumPageComponent implements OnInit {
  album :IAlbum[]=[];
  getState: Observable<any>;
  suscription:Subscription;
  admin: any;

  constructor(private albumService: AlbumService ,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private authService:AuthService,
    private store: Store<AppState>) { 
      this.getState = this.store.select(selectAlbumState);
      this.store.dispatch(new LoadAlbums());
      this.admin =authService.isAdmin();
  }
  
  ngOnInit() {
    //this.album = this.store.select(s => s.albumState.album.album);
    this.albumService.getAlbums().subscribe(
      rests => this.album = rests
    );
    console.log(this.album);
  
   // this.store.dispatch(new LoadAlbums());
  }

  delete(rest: IAlbum) {
   // const i = this.album.indexOf(rest);
   // this.album = this.album.slice(0, i).concat(this.album.slice(i + 1));
   // this.snackBar.open("Album borrado correctamente","Cerrar", {panelClass: ['green-snackbar']})
  }

  goToCreateAlbum(){
    this.router.navigate(['/album/add']);
  }

}
