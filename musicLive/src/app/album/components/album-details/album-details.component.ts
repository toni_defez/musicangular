import { Component, OnInit } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { Router, ActivatedRoute } from '@angular/router';
import { SongService } from 'src/app/song/services/song.service';
import { ISong } from 'src/app/shared/interfaces/song.model';
import { Store } from '@ngrx/store';
import { AppState, selectAlbumState } from 'src/app/store/app.state';
import { SelectedAlbum } from 'src/app/store/actions/album.actions';
import { Observable } from 'rxjs';
import { SelectedSong, AddedSongToQueue } from 'src/app/store/actions/playlist.actions';
import { SelectPlaylistDialogComponent } from 'src/app/shared/dialog/select-playlist-dialog/select-playlist-dialog.component';
import { MatDialog, MatSnackBar } from '@angular/material';
import { fakeAsync } from '@angular/core/testing';
import { AuthService } from 'src/app/auth/services/auth.service';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { AlbumService } from '../../services/album.service';

@Component({
  selector: 'app-album-details',
  templateUrl: './album-details.component.html',
  styleUrls: ['./album-details.component.scss']
})
export class AlbumDetailsComponent implements OnInit {

  album: IAlbum;
  songs: ISong[];
  getState: Observable<any>;
  admin:boolean= false;

  constructor(private router: Router,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    private songService: SongService,
    private authService:AuthService,
    private albumService:AlbumService,
    public snackBar: MatSnackBar,
    private store: Store<AppState>) {
    this.getState = this.store.select(selectAlbumState);

    this.admin =authService.isAdmin();

   
  }

  ngOnInit() {
    this.album = this.route.snapshot.data['album'];
    this.songService.getSongByAlbum(this.album._id).subscribe(songs => this.songs = songs);
    console.log(this.album);
  }

  playAlbum() {
    this.store.dispatch(new SelectedAlbum(this.album));
  }

  playSong(song) {
    this.store.dispatch(new SelectedSong(song));
    console.log(song);
  }

  addSong(){
      this.router.navigate([`/song/add/${this.album._id}`]);
  }

  AddSongPlayList(song:ISong){
    let params = {
      title:song.name,
      _id:song._id
    }
    const dialogRef = this.dialog.open(SelectPlaylistDialogComponent, {
        width:'1000px',
        height:'600px',
        data:{song:params}
      });
  }

  RemoveSong(song:ISong){
    this.songService.deleteSong(song._id).subscribe(resp=>{
      this.songs = this.songs.filter(item=>item._id!=song._id);
      this.snackBar.open("Album uploaded", "close", { panelClass: ['green-snackbar'] })
    });
  }

  AddSongQueue(song:ISong){
    this.store.dispatch(new AddedSongToQueue(song));
  }


  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this album?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.removeAlbum();
      }
    });
  }

  removeAlbum(){
    this.albumService.deleteAlbum(this.album._id).subscribe(
      () => { 
        this.snackBar.open("Album deleted", "close", { panelClass: ['green-snackbar'] });
        this.router.navigateByUrl(`/home`);
       },
      err => {
        console.log("ERROR\n", err)
      }
    )
    
  }

}
