import { Component, OnInit, Input, Output, EventEmitter, Inject } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { AlbumService } from '../../services/album.service';
import { Router } from '@angular/router';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { AppState, selectAlbumState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { SelectedAlbum } from 'src/app/store/actions/album.actions';

@Component({
  selector: 'app-album-card',
  templateUrl: './album-card.component.html',
  styleUrls: ['./album-card.component.scss']
})
export class AlbumCardComponent implements OnInit {

  animal: string;
  name: string;
  getState: Observable<any>;

  @Input() album: IAlbum;
  @Output() deleted = new EventEmitter<void>();

  constructor(private albumService: AlbumService, private router: Router,
    public dialog: MatDialog,
    private store: Store<AppState>) {
      this.getState = this.store.select(selectAlbumState);
     }
  ngOnInit() {

  }

  info() {
    this.router.navigate(['/album', this.album._id]);
  }

  edit() {
    this.router.navigate(['album/edit', this.album._id]);
  }

  playAlbum(){
    this.store.dispatch(new SelectedAlbum(this.album));
    console.log("Reproduceme esta "+ this.album.title);
  }

  delete() {

    this.albumService.deleteAlbum(this.album._id).subscribe(
      () => { this.deleted.emit() },
      err => {
        console.log("ERROR\n", err)
      }
    )

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.delete();
      }
    });
  }



}
