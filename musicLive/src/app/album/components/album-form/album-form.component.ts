import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { UserService } from 'src/app/user/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { requiredFileType } from 'src/app/shared/validators/requiredFileType';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { ArtistService } from 'src/app/artist/services/artist.service';
import { AlbumService } from '../../services/album.service';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { UploadService } from 'src/app/shared/service/upload.service';

export interface State {
  flag: string;
  name: string;
  population: string;
}

@Component({
  selector: 'app-album-form',
  templateUrl: './album-form.component.html',
  styleUrls: ['./album-form.component.css']
})
export class AlbumFormComponent implements OnInit {
  userForm: FormGroup;
  user: IUser;
  error: any;
  artist: IArtist[] = [];
  selectedArtist: IArtist;

  filteredStates: Observable<IArtist[]>;

  constructor(private userService: UserService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private artistService: ArtistService,
    private albumService: AlbumService,
    private uploadService: UploadService,
    public snackBar: MatSnackBar) {
      
     }

  ngOnInit() {
    this.artistService.getAllArtist().subscribe(
      rests => this.artist = rests
    );

    this.userForm = this.fb.group({
      title: ['toni', [Validators.required]],
      description: ['toni', [Validators.required]],
      year: ['', [Validators.required]],
      artist: ["", [Validators.required]],
      image: [null, [Validators.required, requiredFileType('jpg')]],
    });
    this.filteredStates = this.userForm.valueChanges
      .pipe(
        startWith(''),
        map(artist => artist ? this._filterStates(artist) : this.artist.slice())
      );


  }
  get f() { return this.userForm.controls; }

  submit() {
    let dateTime = this.f.year.value;
    let imagen = this.userForm.get('image').value


    if (this.userForm.valid) {

      let params: any = {
        "title": this.f.title.value,
        "description": this.f.description.value,
        "year": dateTime.getFullYear(),
        "artist": this.selectedArtist._id
      }
      this.albumService.addAlbum(<IAlbum>params).subscribe((resp) => {
        if (resp) {
          this.uploadService.uploadAlbum(resp._id, this.userForm.get('image').value).subscribe((resp) => {
            this.snackBar.open("Album uploaded", "close", { panelClass: ['green-snackbar'] });
            this.router.navigateByUrl(`/home`);
          }, error => this.error = error);

        }
      },
        error => {
          this.error = error;
        });
    }
  }

  private _filterStates(value: any): IArtist[] {
    let currentState = value.artist;
    const filterValue = currentState.toLowerCase();

    let elements = this.artist.filter(artist =>
      artist.name.toLowerCase().indexOf(filterValue) === 0);

    this.selectedArtist = elements.length > 0 ? elements[0] : null;
    return elements;
  }

}
