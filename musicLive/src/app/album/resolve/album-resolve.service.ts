import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { Observable, of } from 'rxjs';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { catchError } from 'rxjs/operators';
import { AlbumService } from '../services/album.service';

@Injectable({
  providedIn: 'root'
})
export class AlbumResolve implements Resolve<IAlbum> {

  constructor(private albumService:AlbumService,private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<IAlbum> {
 
    return this.albumService.getAlbum(route.params['id']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
}

 
}
