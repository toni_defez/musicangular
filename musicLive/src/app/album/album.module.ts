import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlbumRoutingModule } from './album-routing.module';
import { AlbumPageComponent } from './components/album-page/album-page.component';
import { AlbumCardComponent } from './components/album-card/album-card.component';
import { SharedModule } from '../shared/shared.module';
import { ConfirmationDialogComponent } from '../shared/components/confirmation-dialog/confirmation-dialog.component';
import { AlbumDetailsComponent } from './components/album-details/album-details.component';
import { AlbumFormComponent } from './components/album-form/album-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [AlbumPageComponent, AlbumCardComponent, AlbumDetailsComponent, AlbumFormComponent],
  imports: [
    CommonModule,
    AlbumRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports:[
    AlbumPageComponent
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
})
export class AlbumModule { }
