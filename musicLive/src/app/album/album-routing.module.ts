import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AlbumPageComponent } from './components/album-page/album-page.component';
import { AlbumDetailsComponent } from './components/album-details/album-details.component';
import { AlbumResolve } from './resolve/album-resolve.service';
import { AlbumFormComponent } from './components/album-form/album-form.component';
import { AdminActivateGuard } from '../guards/admin-activate.guard';

const routes: Routes = [
  {
    path: '',
    component: AlbumPageComponent,
  },
  {
    path:'add',
    component:AlbumFormComponent,
    canActivate: [AdminActivateGuard]
  },
  {
    path: ':id',
    component:AlbumDetailsComponent,
    pathMatch:'full',
    resolve:{
      album:AlbumResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AlbumRoutingModule { }
