import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ArtistRoutingModule } from './artist-routing.module';
import { ArtistPageComponent } from './components/artist-page/artist-page.component';
import { SharedModule } from '../shared/shared.module';
import { ArtistDetailComponent } from './components/artist-detail/artist-detail.component';
import { ArtistAlbumListComponent } from './components/artist-album-list/artist-album-list.component';
import { ArtistAlbumPlayableComponent } from './components/artist-album-playable/artist-album-playable.component';
import { ArtistFormComponent } from './components/artist-form/artist-form.component';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [ArtistPageComponent, ArtistDetailComponent, ArtistAlbumListComponent, ArtistAlbumPlayableComponent, ArtistFormComponent],
  imports: [
    CommonModule,
    ArtistRoutingModule,
    SharedModule,
    ReactiveFormsModule
  ],
  exports:[
    ArtistPageComponent
  ]
})
export class ArtistModule { }
