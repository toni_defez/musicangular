import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class ArtistService {

  constructor(private http: HttpClient) { }

  getAllArtist(): Observable<IArtist[]>{

    return this.http.get<{artists:IArtist[]}>('artist/getAll').pipe(
      map(resp =>
        resp.artists.map(r => {
         r.image = `${environment.baseUrl}/artist/get-image-artist/${r.image}`;
          return r;
        }),
        catchError((resp: HttpErrorResponse) => throwError(`Error register
        user. Status: ${resp.status}. Message: ${resp.message}`))
      )
    );
  }

  addArtist(artist:IArtist): Observable<IArtist> {
    return this.http.post<{ artist: IArtist }>(`artist/save-artist`, artist).pipe(
      map(resp => {
        const r = resp.artist;
        return r;
      })
    );
  }


  deleteArtist(id: string): Observable<void> {
    return this.http.delete<void>(`artist/${id}`);
  }

  
  getArtist(id:string):Observable<IArtist>{
    return this.http.get<{ artist:IArtist }>(`artist/detail/${id}`).pipe(
      map(resp => {
        const r = resp.artist;
        r.image = `${environment.baseUrl}/artist/get-image-artist/${r.image}`;
        return r;
      })
    );
  }


 
}
