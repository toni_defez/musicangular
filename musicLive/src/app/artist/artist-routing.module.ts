import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArtistPageComponent } from './components/artist-page/artist-page.component';
import { ArtistDetailComponent } from './components/artist-detail/artist-detail.component';
import { ArtistResolve } from './resolve/artist-resolve.service';
import { ArtistFormComponent } from './components/artist-form/artist-form.component';
import { AdminActivateGuard } from '../guards/admin-activate.guard';

const routes: Routes = [
  {
    path: '',
    component: ArtistPageComponent,
  },
  {
    path:'add',
    component:ArtistFormComponent,
    canActivate: [AdminActivateGuard]
  },
  {
    path: ':id',
    component:ArtistDetailComponent,
    pathMatch:'full',
    resolve:{
      artist:ArtistResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArtistRoutingModule { }
