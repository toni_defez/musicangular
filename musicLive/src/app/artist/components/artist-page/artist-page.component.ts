import { Component, OnInit } from '@angular/core';
import { ArtistService } from '../../services/artist.service';
import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-artist-page',
  templateUrl: './artist-page.component.html',
  styleUrls: ['./artist-page.component.scss']
})
export class ArtistPageComponent implements OnInit {

  artists: IArtist[];
  topTracks: any[] = [];
  admin:any;

  loadingArtist: boolean;
  constructor(
    private artistService: ArtistService ,
    private route: ActivatedRoute,
    private router: Router,
    public snackBar: MatSnackBar,
    private authService:AuthService
  ) {
    this.admin = this.authService.isAdmin();
   }

  ngOnInit() {
    
    this.artistService.getAllArtist().subscribe(
      rests => this.artists = rests
    );
  }

  openArtist(artist:IArtist){
    this.router.navigate(['/artist', artist._id]);
  }

  goToCreateArtist(){

    this.router.navigate([`/artist/add`]);
  }

}
