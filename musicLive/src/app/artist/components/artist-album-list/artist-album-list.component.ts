import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { ISong } from 'src/app/shared/interfaces/song.model';

@Component({
  selector: 'app-artist-album-list',
  templateUrl: './artist-album-list.component.html',
  styleUrls: ['./artist-album-list.component.css']
})
export class ArtistAlbumListComponent implements OnInit {

  @Input() albums: IAlbum[];
  @Input() artist: IArtist;
  @Output() onAddSong = new EventEmitter();

  constructor() { }

  ngOnInit() {
    console.log(this.albums);
  }

  /**
   * Add a song to a playlist.
   *
   * @param song
   *  song to add .
   */
  addSongToPlaylist(song: ISong) {
    
    this.onAddSong.emit(song);
  }


}
