import { Component, OnInit } from '@angular/core';


import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { UserService } from 'src/app/user/services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { requiredFileType } from 'src/app/shared/validators/requiredFileType';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { ArtistService } from 'src/app/artist/services/artist.service';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { UploadService } from 'src/app/shared/service/upload.service';


@Component({
  selector: 'app-artist-form',
  templateUrl: './artist-form.component.html',
  styleUrls: ['./artist-form.component.css']
})
export class ArtistFormComponent implements OnInit {

  userForm: FormGroup;
  error:any;
  progress=0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private artistService: ArtistService,
    private uploadService: UploadService,
    public snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    this.userForm = this.fb.group({
      name: ['toni', [Validators.required]],
      description: ['toni', [Validators.required]],
      image: [null, [Validators.required, requiredFileType('jpg')]],
    });
  }

  get f() { return this.userForm.controls; }

  submit() {

    let imagen = this.userForm.get('image').value;
    if (this.userForm.valid) {
      let params: any = {
        "name": this.f.name.value,
        "description": this.f.description.value
      }
      this.artistService.addArtist(<IArtist>params).subscribe((resp) => {
        if (resp) {
          this.uploadService.uploadArtist(resp._id, this.userForm.get('image').value).subscribe((resp) => {
            this.router.navigateByUrl(`/home`);
            this.snackBar.open("Artist uploaded", "close", { panelClass: ['green-snackbar'] })
          }, error => this.error = error);
        }
      });
    }
  }

}
