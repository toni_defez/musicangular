import { Component, OnInit } from '@angular/core';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { Router, ActivatedRoute } from '@angular/router';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { AlbumService } from 'src/app/album/services/album.service';
import { Store } from '@ngrx/store';
import { AppState, selectAlbumState } from 'src/app/store/app.state';
import { SelectedAlbum } from 'src/app/store/actions/album.actions';
import { Observable } from 'rxjs';
import { ISong } from 'src/app/shared/interfaces/song.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { SelectedArtistPlaylist } from 'src/app/store/actions/playlist.actions';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { ArtistService } from '../../services/artist.service';
import { AuthService } from 'src/app/auth/services/auth.service';

@Component({
  selector: 'app-artist-detail',
  templateUrl: './artist-detail.component.html',
  styleUrls: ['./artist-detail.component.scss']
})
export class ArtistDetailComponent implements OnInit {

  artist:IArtist;
  albums:IAlbum[];
  getState: Observable<any>;
  admin:any;
  constructor(private router: Router,
    private route: ActivatedRoute,private albumService:AlbumService,
    private artistService:ArtistService,
    public snackBar: MatSnackBar,
    private store: Store<AppState>,public dialog: MatDialog,
    private authService:AuthService) {
      this.admin = authService.isAdmin();
      this.getState = this.store.select(selectAlbumState);
     }

  ngOnInit() {
    this.artist = this.route.snapshot.data['artist'];

    this.albumService.getAlbumByArtist(this.artist._id).subscribe
    (album=>{
      this.albums = album;
    });
  }

  addSongToPlaylist(event){
    console.log(event);
  }

  playAll(){
    let array = [];
    this.albums.forEach(item=>{
      array.push(...item.songsOfAlbum);
    });
    this.store.dispatch(new SelectedArtistPlaylist(array));
  }

  playAlbum() {
    this.store.dispatch(new SelectedAlbum(this.albums));
  }

  playSong(song) {
    console.log(song);
  }

  removeArtist(){
    this.artistService.deleteArtist(this.artist._id).subscribe(
      () => { 
        this.snackBar.open("Artist deleted", "close", { panelClass: ['green-snackbar'] });
        this.router.navigateByUrl(`/home`);
       },
      err => {
        console.log("ERROR\n", err)
      }
    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this artist?"
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log('Yes clicked');
        this.removeArtist();
      }
    });
  }





}
