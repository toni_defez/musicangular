import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArtistAlbumPlayableComponent } from './artist-album-playable.component';

describe('ArtistAlbumPlayableComponent', () => {
  let component: ArtistAlbumPlayableComponent;
  let fixture: ComponentFixture<ArtistAlbumPlayableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArtistAlbumPlayableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArtistAlbumPlayableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
