import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, State } from '@ngrx/store';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { IArtist } from 'src/app/shared/interfaces/artist.model';
import { ISong } from 'src/app/shared/interfaces/song.model';
import { AppState, selectAlbumState } from 'src/app/store/app.state';
import { SelectedAlbum } from 'src/app/store/actions/album.actions';
import { SelectedSong, AddedSongToQueue } from 'src/app/store/actions/playlist.actions';
import { SelectPlaylistDialogComponent } from 'src/app/shared/dialog/select-playlist-dialog/select-playlist-dialog.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-artist-album-playable',
  templateUrl: './artist-album-playable.component.html',
  styleUrls: ['./artist-album-playable.component.css']
})
export class ArtistAlbumPlayableComponent implements OnInit {

  @Input() album: any;
  @Input() artist: IArtist;
  @Output() onAddSong = new EventEmitter();

  songs: ISong[];
  info: string;
  getState: Observable<any>;


  constructor( private store: Store<AppState>, public dialog: MatDialog,) {
    this.getState = this.store.select(selectAlbumState);
   }

  ngOnInit() {
    // Start fetching songs.
    this.songs = this.album.songsOfAlbum;
  
  }

  /**
   * Handler to play a song.
   */
  playSong(song: ISong) {
    console.log("pillado"+song);
    this.store.dispatch(new SelectedSong(song));
   // this.albumService.playSong(song);
  }

  /**
   * Play this album
   */
  playAlbum() {
    this.store.dispatch(new SelectedAlbum(this.album.album));
    //this.albumService.playAlbum(this.album, this.songs);
  }

  /**
   * Add song to a playlist.
   */
  addSongToPlaylist(song: ISong) {
    this.onAddSong.emit(song);
  }

  AddSongPlayList(song:ISong){
    let params = {
      title:song.name,
      _id:song._id
    }
    const dialogRef = this.dialog.open(SelectPlaylistDialogComponent, {
        width:'1000px',
        height:'600px',
        data:{song:params}
      });
  }

  AddSongQueue(song:ISong){
    this.store.dispatch(new AddedSongToQueue(song));
  }

}
