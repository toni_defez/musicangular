import { Injectable } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { ArtistService } from '../services/artist.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IArtist } from 'src/app/shared/interfaces/artist.model';

@Injectable({
  providedIn: 'root'
})
export class ArtistResolve implements Resolve<IArtist>  {

  constructor(private artistService:ArtistService,
    private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<IArtist> {
 
    return this.artistService.getArtist(route.params['id']).pipe(
      catchError(error => {
        console.log(error);
        this.router.navigate(['/home']);
        return of(null);
      })
    );
  }
}
