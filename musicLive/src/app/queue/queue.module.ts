import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { QueueRoutingModule } from './queue-routing.module';
import { QueueListComponent } from './components/queue-list/queue-list.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [QueueListComponent],
  imports: [
    CommonModule,
    QueueRoutingModule,
    SharedModule
  ]
})
export class QueueModule { }
