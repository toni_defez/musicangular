import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QueueListComponent } from './components/queue-list/queue-list.component';

const routes: Routes = [
  { path: '', component: QueueListComponent, pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QueueRoutingModule { }
