import { Component, OnInit } from '@angular/core';
import { ISong } from 'src/app/shared/interfaces/song.model';
import { Observable, Subscription } from 'rxjs';
import { SongService } from 'src/app/song/services/song.service';
import { AlbumService } from 'src/app/album/services/album.service';
import { Store } from '@ngrx/store';
import { AppState, selectedPlayListState } from 'src/app/store/app.state';
import { AudioPlayerService } from 'src/app/shared/player-module/service/audio-player.service';
import { PlayListActionTypes, RemoveAllSongs } from 'src/app/store/actions/playlist.actions';

@Component({
  selector: 'app-queue-list',
  templateUrl: './queue-list.component.html',
  styleUrls: ['./queue-list.component.scss']
})
export class QueueListComponent implements OnInit {
  queue:  ISong[];
  getStatePlayList: Observable<any>;
  subscriptionAlbum:Subscription;

  constructor(songService:SongService,albumService:AlbumService,
    private store: Store<AppState>,private audioPlayerservice:AudioPlayerService) {
      this.getStatePlayList = this.store.select(selectedPlayListState);
    }
  ngOnInit() {

    this.subscriptionAlbum = this.getStatePlayList.subscribe((state) => {
      console.log("ahhhhhhh");
      this.queue = state.songs;
    });

    // this.queue = this.store.select(s=>s.playListState.songs);
  }

  playSong(song: ISong) {
    this.audioPlayerservice.selectSong(song);
  }

  removeSongFromQueue(song: ISong) {

    this.queue = this.queue.filter(item=>item._id!=song._id);
    this.store.dispatch({
      type: PlayListActionTypes.RemoveSong,
      payload: {
        song: song,
      }
    });
    this.audioPlayerservice.removeSong(song);
  }
  removeAll() {
    this.queue = [];
    this.store.dispatch(new RemoveAllSongs({}));
    this.audioPlayerservice.removeAll();
   /**
    this.store.dispatch({
      type: PlayListActionTypes.RemoveAllSong,
      payload: {}
    });
    **/
  }
}
