import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth/services/auth.service';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { AppState, selectAuthState } from './store/app.state';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent  implements OnInit{
  title = 'musicLive';
  logged:boolean ;
  getState: Observable<any>;
  isAuthenticated: false;
  user = null;
  errorMessage = null;

  constructor( private authService: AuthService,
    private store: Store<AppState>) {
      this.getState = this.store.select(selectAuthState);
    this.authService.loginChange$.subscribe(logged => {
      this.logged = logged;
    });
  }
  ngOnInit(): void {
    
    this.getState.subscribe((state) => {
      this.isAuthenticated = state.isAuthenticated;
      this.user = state.user;
      this.errorMessage = state.errorMessage;
    });
  }
}
