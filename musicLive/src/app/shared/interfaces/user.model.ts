export interface IUser{
    _id:string,
    name:string,
    surname:string,
    email:string,
    password:string,
    image:string,
    role:string,
    token:string
}