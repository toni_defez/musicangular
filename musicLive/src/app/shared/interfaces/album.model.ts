import { IArtist } from './artist.model';
import { ISong } from './song.model';

export interface IAlbum{
    _id:string,
    title:string,
    description:string,
    year:number,
    artist:IArtist,
    image:string,
    songsOfAlbum:ISong[]
}