import { IAlbum } from './album.model';

export interface ISong{
    _id:string,
    number:String,
    name:String,
    duration:String,
    file:String,
    album:IAlbum
}