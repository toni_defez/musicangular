import { IUser } from './user.model';
import { ISong } from './song.model';

export interface IPlaylist{
    _id:string,
    userCreator:IUser,
    name:string,
    songs:ISong[],
    isPrivacy:boolean,
    file:string
}