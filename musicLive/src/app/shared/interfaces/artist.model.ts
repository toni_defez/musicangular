export interface IArtist{
    name:string,
    description:string,
    image:string,
    _id:string,
}