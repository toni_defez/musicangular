import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { BaseAudioPlayerFunctions } from '../baseplayer/baseplayer.component';
import { MatSlider, MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { Track } from '../model/track.model';
import { AudioPlayerService } from '../service/audio-player.service';
import { SelectPlaylistDialogComponent } from '../../dialog/select-playlist-dialog/select-playlist-dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-musicplayer',
  templateUrl: './musicplayer.component.html',
  styleUrls: ['./musicplayer.component.scss']
})
export class MusicplayerComponent extends BaseAudioPlayerFunctions implements OnInit {

    timeLineDuration: MatSlider;
    dataSource = new MatTableDataSource<Track>();
    playlistData: Track[];
    playlistTrack: any;

    constructor(private playlistService: AudioPlayerService,
         public dialog: MatDialog,
         private router: Router) {
        super();
    }

    ngOnInit() {
  
        this.bindPlayerEvent();
        this.player.nativeElement.addEventListener('ended', () => {
            if (this.checkIfSongHasStartedSinceAtleastTwoSeconds()) {
                this.nextSong();
            }
        });
        this.playlistService.setPlaylist(this.playlistData);
        this.playlistService.getSubjectCurrentTrack().subscribe((playlistTrack) => {
            this.playlistTrack = playlistTrack;
        });
        this.player.nativeElement.currentTime = 0;
        this.playlistService.init();
    }
    

    nextSong(): void {
        this.currentTime = 0;
        this.duration = 0.01;
        this.playlistService.nextSong();
        this.play();
    };

    previousSong(): void {
        this.currentTime = 0;
        this.duration = 0.01;
        if (!this.checkIfSongHasStartedSinceAtleastTwoSeconds()) {
            this.playlistService.previousSong();
        } else {
            this.resetSong();
        }
        this.play();
    };

    resetSong(): void {
        this.player.nativeElement.src = this.playlistTrack[1].link;
    };

    selectTrack(index: number): void {
        console.log('selectTrack(index: number): void: ' + index);
        this.playlistService.selectATrack(index);
        setTimeout(() => {
            this.player.nativeElement.play();
        }, 0);
    };

    checkIfSongHasStartedSinceAtleastTwoSeconds(): boolean {
        return this.player.nativeElement.currentTime > 2;
    };

    @Input()
    set playlist(playlist: Track[]) {
        this.playlistData = playlist;
        this.ngOnInit();
    }

    AddSongPlayList(){
        //seleccionar la cancion en reproduccion
        let track =this.playlistData[this.playlistService.getIndexSong()];
        
        const dialogRef = this.dialog.open(SelectPlaylistDialogComponent, {
            width:'1000px',
            height:'600px',
            data:{song:track}
          });

    }
    goToAlbum(){
        let track:Track =this.playlistData[this.playlistService.getIndexSong()]; 

        this.router.navigate(['/album', track.albumId]);
    }
}
