export class Track {
    index?: number;
    link: string;
    title?: string;
    image?:string;
    albumId?:string;
    _id:string;
}