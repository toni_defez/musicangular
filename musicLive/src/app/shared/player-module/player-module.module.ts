import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseAudioPlayerFunctions } from './baseplayer/baseplayer.component';
import { MusicplayerComponent } from './musicplayer/musicplayer.component';
import { SecondsToMinutesPipe } from './pipe/seconds-to-minute.pipe';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faPlay, faPause, faSpinner, faStepForward, faStepBackward
} from '@fortawesome/free-solid-svg-icons';
import { MatButtonModule, MatCardModule, MatTableModule, MatFormFieldModule, MatSliderModule, MatExpansionModule, MatPaginatorModule, MatIconModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import {MatMenuModule} from '@angular/material/menu';
import { SelectPlaylistDialogComponent } from '../dialog/select-playlist-dialog/select-playlist-dialog.component';
import { ConfirmationDialogComponent } from '../components/confirmation-dialog/confirmation-dialog.component';
@NgModule({
  declarations: [BaseAudioPlayerFunctions, MusicplayerComponent, SecondsToMinutesPipe],
  imports: [
    CommonModule, MatButtonModule, MatCardModule, MatTableModule, MatFormFieldModule, 
    MatSliderModule, MatExpansionModule, MatPaginatorModule, FormsModule, FontAwesomeModule,
    MatIconModule,MatMenuModule
  ],
  exports:[
    MusicplayerComponent
  ],
  entryComponents:[
    SelectPlaylistDialogComponent,
    ConfirmationDialogComponent
  ]
})
export class PlayerGeneralModule {
  constructor() {
    library.add(faPlay, faPause, faSpinner, faStepForward, faStepBackward);
  }
 }
