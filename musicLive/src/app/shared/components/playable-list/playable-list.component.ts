import { Component, OnInit, Input, Output, EventEmitter, ContentChild, TemplateRef } from '@angular/core';
import { ISong } from '../../interfaces/song.model';

@Component({
  selector: 'app-playable-list',
  templateUrl: './playable-list.component.html',
  styleUrls: ['./playable-list.component.scss']
})
export class PlayableListComponent implements OnInit {

  @Input() image: string;
  @Input() title: string;
  @Input() info: string;
  @Input() playAllExists = true;
  @Input() removeAllExists = false;
  @Input() songs: ISong[];

  @Output() onPlayall = new EventEmitter();
  @Output() onPlayItem = new EventEmitter();
  @Output() onRemoveAll = new EventEmitter();

  @ContentChild(TemplateRef) itemTemplate: TemplateRef<any>;

  constructor() { }

  ngOnInit() {
  }

  /**
   * Play all items.
   */
  playAll() {
    this.onPlayall.emit();
  }

  /**
   * Play a single item.
   */
  playItem(song: ISong) {
    this.onPlayItem.emit(song);
  }

  /**
   * Play all items.
   */
  removeAll() {
    this.onRemoveAll.emit();
  }
}
