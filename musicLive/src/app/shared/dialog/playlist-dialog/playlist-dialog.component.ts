import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators,ReactiveFormsModule } from '@angular/forms';
import { UploadService } from '../../service/upload.service';
import { IUser } from '../../interfaces/user.model';
import { requiredFileType } from '../../validators/requiredFileType';
import { PlaylistService } from 'src/app/playlist/services/playlist.service';
import { Store } from '@ngrx/store';
import { AppState, selectAuthState } from 'src/app/store/app.state';
import { Subscription } from 'rxjs';

export interface DialogData {
  user: IUser;
  list:any
}

@Component({
  selector: 'app-playlist-dialog',
  templateUrl: './playlist-dialog.component.html',
  styleUrls: ['./playlist-dialog.component.css']
})

export class PlaylistDialogComponent implements OnInit  {
 
 
  form:FormGroup;
  user:IUser;
  getState: any;
  suscription:Subscription;

  constructor(
    public dialogRef: MatDialogRef<PlaylistDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private uploadService: UploadService,
    private playlistService:PlaylistService,
    private fb:FormBuilder,
    private store: Store<AppState>
    ) {
      this.getState = this.store.select(selectAuthState);
      this.form = this.fb.group({
        name: ['',Validators.required],
        image: [null, [Validators.required, requiredFileType('jpg')]]
      }
      );
    }

    ngOnInit(): void {
      this.suscription = this.getState.subscribe((state) => {
        this.user = state.client;
      })
    }

    get f() { return this.form.controls; }


  close(): void {
    this.dialogRef.close();
  }

  submit() {
    if (this.form.valid) {
      let params:any = {
        name:this.f.name.value,
      }
      

      this.playlistService.addPlayList(this.user._id,params).subscribe((resp)=>{
        if(resp){
          this.uploadService.uploadPlayList(resp._id, this.form.get('image').value).subscribe((resp:any) => {
          this.dialogRef.close(resp.playlist);
          }, error => console.log("error"))
        }
        else{
          console.log("Error");
        }
      })
    }
    console.log("formulacion");
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.suscription.unsubscribe();
  }
}
