import { Component, OnInit, Inject } from '@angular/core';
import { ISong } from '../../interfaces/song.model';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog, MatSnackBar } from '@angular/material';
import { IPlaylist } from '../../interfaces/playlist.model';
import { Store } from '@ngrx/store';
import { AppState, selectedListSong,  } from 'src/app/store/app.state';
import { Observable, Subscription } from 'rxjs';
import {ChangeDetectionStrategy, ViewEncapsulation} from '@angular/core';
import { ConfirmationDialogComponent } from '../../components/confirmation-dialog/confirmation-dialog.component';
import { Track } from '../../player-module/model/track.model';
import { PlaylistService } from 'src/app/playlist/services/playlist.service';


export interface DialogData {
  song: Track;
}

@Component({
  selector: 'app-select-playlist-dialog',
  templateUrl: './select-playlist-dialog.component.html',
  styleUrls: ['./select-playlist-dialog.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectPlaylistDialogComponent implements OnInit {

  playlist:IPlaylist[];
  getState: Observable<any>;
  subscriptionPlayList:Subscription;
  selectedSong:any;

   
  modalIsOpen = '';
  modalTitle = 'scroll to update';

  
  modalScrollDistance = 2;
  modalScrollThrottle = 50;



  constructor(
    public dialogRef: MatDialogRef<SelectPlaylistDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private store: Store<AppState>,
    public snackBar: MatSnackBar,
    public dialog: MatDialog,private playlistService:PlaylistService) { 
      this.selectedSong = data.song;
     this.getState = this.store.select(selectedListSong);
  }

  ngOnInit() {
    this.subscriptionPlayList = this.getState.subscribe((state)=>{
      this.playlist = state.MEplaylist;
    });
  }

  onScrollDown () {
    console.log('scrolled!!');

    
  }
  
  onModalScrollDown () {
    this.modalTitle = 'updated on ' + (new Date()).toString();

  }
  addSong(item:IPlaylist){
    console.log(this.data);

    let question = `¿Quieres añadir ${this.selectedSong.title} a tu lista  ${item.name}?`;
  
      const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
        width: '350px',
        data: question
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.playlistService.postSongToPlayList(item,this.selectedSong._id).subscribe(
            resp=>{
              console.log("todo bien !!");

              this.snackBar.open(`${this.selectedSong.title} added to  ${item.name}`, "close", { panelClass: ['green-snackbar'] })
              this.dialogRef.close();
            }
          )
          
        }
      });
    }

  

  

}
