import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelectPlaylistDialogComponent } from './select-playlist-dialog.component';

describe('SelectPlaylistDialogComponent', () => {
  let component: SelectPlaylistDialogComponent;
  let fixture: ComponentFixture<SelectPlaylistDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPlaylistDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelectPlaylistDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
