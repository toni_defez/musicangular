import { Injectable } from '@angular/core';

import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  httpOptions: any;
  constructor(private http: HttpClient) { 
   }

  public upload(id,file) {
    const formData: FormData = new FormData();
    formData.append('image', file);
    return this.http.post(`user/upload-image-user/${id}`, formData);
  }

  public uploadAlbum(id,file){
   
    const formData: FormData = new FormData();
    formData.append('image', file);
    return this.http.post(`album/upload-image-album/${id}`, formData);
  }

  public uploadSong(id,file){
    console.log("upload song");
    const formData: FormData = new FormData();
    formData.append('file', file);
    return this.http.post(`song/upload-file-song/${id}`, formData);
  }


  public uploadArtist(id,file){
    console.log("upload artist");
    const formData: FormData = new FormData();
    formData.append('image', file);
    return this.http.post(`artist/upload-image-artist/${id}`, formData);
  }
  

  public uploadPlayList(id,file){
   
    const formData: FormData = new FormData();
    formData.append('image', file);
    return this.http.post(`list/upload-image-playlist/${id}`, formData);
  }



}
