import {MatButtonModule,MatCheckboxModule, MatCard, MatNativeDateModule} from '@angular/material';
import { NgModule } from '@angular/core';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {
     MatSidenavModule,
      MatListModule,
       MatDialogModule, MatInputModule, MatTableModule,
      MatProgressSpinnerModule } from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import {MatTabsModule} from '@angular/material/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ScrollDispatchModule} from '@angular/cdk/scrolling';



@NgModule({
    imports:[
        MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatSidenavModule,
        MatListModule,
        MatCardModule,
        MatDialogModule,
        MatInputModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        FlexLayoutModule,
        MatGridListModule,
        MatSnackBarModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        ScrollDispatchModule,
        
        MatNativeDateModule
    ],
    exports:[
        MatButtonModule,
        MatCheckboxModule,
        MatToolbarModule,
        MatIconModule,
        MatMenuModule,
        MatSidenavModule,
        MatListModule,
        MatCardModule,
        MatDialogModule,
        MatInputModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatTabsModule,
        FlexLayoutModule,
        MatGridListModule,
        MatSnackBarModule,
        MatChipsModule,
        MatAutocompleteModule,
        MatDatepickerModule,
        MatNativeDateModule,
        ScrollDispatchModule
    ]
})
export class MaterialModule{}