import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material';
import { StoreModule } from '@ngrx/store';

import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';


import { ConfirmationDialogComponent } from './components/confirmation-dialog/confirmation-dialog.component';
import { ResponsiveColsDirective } from './directive/responsive-cols.directive';
import { FileUploadComponent } from './components/file-upload/file-upload.component';
import { ProgressComponent } from './components/progress/progress.component';
import { PlayerComponent } from './player/player.component';
import {  PlayerGeneralModule } from './player-module/player-module.module';
import { ViewerBoxComponent } from './components/viewer-box/viewer-box.component';
import { UserBoxComponent } from './components/user-box/user-box.component';
import { IconBoxComponent } from './components/icon-box/icon-box.component';
import { IconsButtonsComponent } from './components/icons-buttons/icons-buttons.component';
import { PlayableListComponent } from './components/playable-list/playable-list.component';
import { PlaylistDialogComponent } from './dialog/playlist-dialog/playlist-dialog.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectPlaylistDialogComponent } from './dialog/select-playlist-dialog/select-playlist-dialog.component';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
@NgModule({
  declarations: [
    ConfirmationDialogComponent,
    ResponsiveColsDirective,
    FileUploadComponent,
    ProgressComponent,
    PlayerComponent,
    ViewerBoxComponent,
    UserBoxComponent,
    IconBoxComponent,
    IconsButtonsComponent,
    PlayableListComponent,
    PlaylistDialogComponent,
    SelectPlaylistDialogComponent

  ],
  imports: [
    CommonModule,
    MaterialModule,
    PlayerGeneralModule,
    ReactiveFormsModule,
    InfiniteScrollModule
  ],
  exports:[
    MaterialModule,
    ResponsiveColsDirective,
    FileUploadComponent,
    ProgressComponent,
    PlayerComponent,
    PlayerGeneralModule,
    ViewerBoxComponent,
    UserBoxComponent,
    IconBoxComponent,
    IconsButtonsComponent,
    PlayableListComponent,
    InfiniteScrollModule
  ]
})
export class SharedModule { }
