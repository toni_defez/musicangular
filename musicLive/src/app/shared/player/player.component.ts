import { Component, OnInit } from '@angular/core';
import { ISong } from '../interfaces/song.model';
import { IAlbum } from '../interfaces/album.model';
import { SongService } from 'src/app/song/services/song.service';

import { AlbumService } from 'src/app/album/services/album.service';
import { Track } from '../player-module/model/track.model';
import { AppState, selectAlbumState,selectedPlayListState } from 'src/app/store/app.state';
import { Store } from '@ngrx/store';
import { Observable, Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.scss']
})
export class PlayerComponent implements OnInit {
  song:ISong;
  album:IAlbum=undefined;
  songs:ISong[];
  getStatePlayList: Observable<any>;
  msaapPlaylist: Track[];
  subscriptionAlbum:Subscription;
  subscriptionPlayList:Subscription;

  constructor(songService:SongService,albumService:AlbumService,
    private store: Store<AppState>) {
    this.getStatePlayList = this.store.select(selectedPlayListState);
   }

  ngOnInit() {
    this.subscriptionAlbum = this.getStatePlayList.subscribe((state) => {

      this.songs = state.songs;
      console.log(this.songs);

      //TODO ARREGLAR ESTO Y NO MORIR
      //DIOS ....
      
      if(this.songs.length>0 && this.songs[0].album._id!=undefined){

        this.msaapPlaylist = this.songs.map((item,index)=>{
          if(!item.file.includes("/song/get-file-song/"))
            item.file =  `${environment.baseUrl}/song/get-file-song/${item.file}`;
          if(!item.album.image.includes("/album/get-image-album/"))
            item.album.image = `${environment.baseUrl}/album/get-image-album/${item.album.image}`;
          let newItem = new Track();
          newItem.link = item.file+"";
          newItem.title = item.name+"";
          newItem.image = item.album.image;
          newItem.index = index;
          newItem._id = item._id;
          newItem.albumId = item.album._id;
          return newItem;
        });
      }
      
    });

  }

  ngOnDestroy(): void {
    this.subscriptionAlbum.unsubscribe();
  }

}
