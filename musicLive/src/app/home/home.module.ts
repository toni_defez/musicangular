import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomePageComponent } from './components/home-page/home-page.component';
import { SearchPageComponent } from './components/search-page/search-page.component';
import { SharedModule } from '../shared/shared.module';
import { AlbumModule } from '../album/album.module';
import { ArtistModule } from '../artist/artist.module';

@NgModule({
  declarations: [HomePageComponent, SearchPageComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SharedModule,
    AlbumModule,
    ArtistModule
  ]
})
export class HomeModule { }
