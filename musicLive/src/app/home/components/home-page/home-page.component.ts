import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, selectedPlayListState, selectAuthState } from 'src/app/store/app.state';
import { Observable, Subscription } from 'rxjs';

import { AuthService } from 'src/app/auth/services/auth.service';
import { UserLoad } from 'src/app/store/actions/auth.actions';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {
  getState: Observable<any>;
  suscription:Subscription;

  constructor(private store: Store<AppState>,authService:AuthService) { 
    this.getState = this.store.select(selectAuthState);
    this.store.dispatch(new UserLoad());
   
  }

  ngOnInit() {
    this.suscription = this.getState.subscribe((state) => {
    });
  }

}
