import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LogoutActivateGuard } from './guards/logout-activate.guard';
import { LoginActivateGuard } from './guards/login-activate.guard';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: './home/home.module#HomeModule',
    canActivate: [LoginActivateGuard]
  },
  {
     path: 'auth',
      loadChildren: './auth/auth.module#AuthModule',
      canActivate: [LogoutActivateGuard]
  },
  { 
    path: 'album',
     loadChildren: './album/album.module#AlbumModule',
     canActivate: [LoginActivateGuard]
  },
  {
    path:'artist',
    loadChildren: './artist/artist.module#ArtistModule',
    canActivate: [LoginActivateGuard]
  },
  { 
    path: 'profile',
    loadChildren:'./user/user.module#UserModule',
    canActivate: [LoginActivateGuard]
  },
  {
    path: 'playlists', 
    loadChildren: './playlist/playlist.module#PlaylistModule',
    canActivate: [LoginActivateGuard]
  },
  { 
    path: 'queue',
    loadChildren: './queue/queue.module#QueueModule',
    canActivate: [LoginActivateGuard]
  },
  {
    path:'song',
    loadChildren:'./song/song.module#SongModule',
    canActivate:[LoginActivateGuard]
  },
  { 
    path: '',
    redirectTo: '/auth/login',
    pathMatch: 'full'
  },
  {
     path: '**', 
     redirectTo: '/auth/login', 
     pathMatch: 'full' 
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
