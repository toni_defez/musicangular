import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { UserService } from 'src/app/user/services/user.service';
import { AppState, selectAuthState } from 'src/app/store/app.state';
import { LogIn } from 'src/app/store/actions/auth.actions';



@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {
  form:FormGroup;
  error:boolean;
  count$: Observable<number>;
  loading = false;
  submitted = false;
  getState: Observable<any>;
  errorMessage: string | null;

  subscription:Subscription

  constructor(private fb:FormBuilder,
    private userService: AuthService, private router: Router,
    private profileService:UserService,
    private store: Store<AppState>)  { 
      this.form = this.fb.group({
        email:['luis@gmail.com', [Validators.required,Validators.email]],
        password: ['123123123', [Validators.required,Validators.minLength(5)]],
      });
      this.getState = this.store.select(selectAuthState);
    }

  get f() { return this.form.controls; }

  goRegister(){
    this.router.navigateByUrl('/auth/register');
  }

  ngOnInit() { 
    this.subscription = this.getState.subscribe((state) => {
      this.errorMessage = state.errorMessage;
      console.log(this.errorMessage);
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  submit() {
    this.submitted = true;
    if(this.form.invalid){
      return;
    }
    if (this.form.valid) {
     this.store.dispatch(new LogIn(this.form.value));
    }
  }
}
