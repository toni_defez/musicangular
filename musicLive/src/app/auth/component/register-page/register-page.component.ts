import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, Form, AbstractControl, ValidationErrors } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { requiredFileType } from 'src/app/shared/validators/requiredFileType';
import { UploadService } from 'src/app/shared/service/upload.service';
import { Store } from '@ngrx/store';
import { AppState, selectAuthState } from 'src/app/store/app.state';
import { Signup } from 'src/app/store/actions/auth.actions';
import { Observable } from 'rxjs';
import { getFormValidationErrors } from 'src/app/shared/validators/getFormValidationErrors';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  form:FormGroup;
  error:string[]=[];
  progress = 0;
  getState: Observable<any>;
  errorMessage: string | null;
  submitted = false;


  constructor(private fb:FormBuilder,
    private userService: AuthService, private router: Router,
    private uploadService: UploadService,
    private store: Store<AppState>) {
    this.form = this.fb.group({
      name: ['',Validators.required],
      surname: ['', Validators.required],
      email:['', [Validators.required,Validators.email]],
      password: ['', [Validators.required,Validators.minLength(5)]],
      passwordConfirm:['',[Validators.required,Validators.minLength(5)]],
      image: [null, [Validators.required, requiredFileType('jpg')]],
    },{validators:this.passWordValidator});
    this.getState = this.store.select(selectAuthState);
   }

  ngOnInit() {
    this.getState.subscribe((state) => {
      this.error=[state.errorMessage];
    });
  }

  submit() {
    
    this.submitted = true;
    if (this.form.valid) {
      
      this.error = undefined;
      let params = {
        name:this.form.value.name,
        surname:this.form.value.surname,
        email:this.form.value.email,
        password:this.form.value.password,
        image:this.form.get('image').value
      }
      this.store.dispatch(new Signup(params));
    }
    else{
      if(this.form.errors && this.form.errors.mismatch){
        this.error = ["Por favor repita su contraseña"];
      }
      else{
        this.error = [];
      }
      console.log(this.form.errors);
    }
  }

  get f() { return this.form.controls; }

  passWordValidator(group: AbstractControl) {
    const pass1 = group.get('password');
    pass1.clearValidators();
    const pass2 = group.get('passwordConfirm');
    return pass1.value === pass2.value ? null : {mismatch: true};
 }

  goLogin(){
    this.router.navigateByUrl('/auth/login');
  }

}
