import { Injectable, EventEmitter } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { IUser } from 'src/app/shared/interfaces/user.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  logged:boolean = false;
  loginChange$ = new EventEmitter<boolean>();
  constructor(private http: HttpClient) { }

  private setLogged(logged: boolean) {
    this.logged = logged;
    this.loginChange$.emit(logged);
  }

  register(user :IUser):Observable<IUser>{
    return this.http.post<{user:any}>(`auth/register`,user).pipe(
      map(resp=>{
        const r = resp.user;
        localStorage.setItem('user', r);
        return r;
      }),
      catchError((resp: HttpErrorResponse) => throwError(`Error register
        user. Status: ${resp.status}. Message: ${resp.message}`))
    )
  }

  login(user:IUser,getHash = true): Observable<any> {

    let params:any= {
      email:user.email,
      password:user.password,
    }
    params = getHash==null?params:
      {email:user.email,
      password:user.password,
      gethash:getHash};
  
    console.log(params);
    return this.http.post<{token: string}>('auth/login',
     params).pipe(
      map(r => {
        localStorage.setItem('fs-token', r.token);
       
        this.setLogged(true);
        return r;
     
      }),
      catchError((resp: HttpErrorResponse) => throwError(`Error login user. Status: ${resp.status}. Message: ${resp.error.message}`))
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.setLogged(false);
  }

  isLogged(): Observable<boolean> {
    if (this.logged) { return of(true); }
    if (!localStorage.getItem('token')) { return of(false); }
    return this.http.get('auth/token').pipe(map(() => {
      console.log();
      this.setLogged(true);
      return true;
    }), catchError(error => of(false)));
  }


  //metodos para redux

  logIn(user:IUser): Observable<any> {

    let params:any= {
      email:user.email,
      password:user.password,
      gethash:true
    }
   
    console.log(params);
    return this.http.post<{user: IUser}>('auth/login',
     params);
  }

  signUp(user:any): Observable<any> {
    return this.http.post<{user:any}>(`auth/register`,user);
  }

  isAdmin():boolean{
     //Provisional
     const user = JSON.parse(localStorage.getItem('user'));
     return user.role == 'ROLE_ADMIN'
  }


}
