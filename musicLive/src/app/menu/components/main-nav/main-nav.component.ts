import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { IUser } from 'src/app/shared/interfaces/user.model';


interface AppState{
  user:any;
  count:number,
  name:string
}

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.scss']
})
export class MainNavComponent {

  logged:boolean ;
  user:any;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
     private router: Router,
     private route: ActivatedRoute,
     private store: Store<AppState>) {
      this.logged = false;
     }

    
  ngOnInit(): void {
    this.user = {
      _id:"123",
      name:"123",
      surname:"123",
      email:"123",
      password:"123",
      image:"123",
      role:"123S"
    }

    this.authService.loginChange$.subscribe(logged => {
      this.logged = logged;
      console.log("LOGIN",this.logged);
    });

    this.store.subscribe(state=>{
      this.user =<IUser> state.user;
    });
  }

  logout() {
    this.authService.logout();
    this.router.navigate(['/auth/login']);
  }

  
  

}
