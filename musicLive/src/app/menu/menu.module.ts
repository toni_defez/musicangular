import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';
import { SidebarComponent } from './components/sidebar/sidebar.component';


@NgModule({
  declarations: [MainNavComponent, SidebarComponent],
  imports: [
    CommonModule,
    SharedModule,
    RouterModule

  ], exports: [
    MainNavComponent
  ]
})
export class MenuModule { }
