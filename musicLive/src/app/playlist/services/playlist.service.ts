import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PlaylistService {

  constructor(private http: HttpClient) { }

  //Create playlist
  addPlayList(id:string,params:any):Observable<IPlaylist>{
    return this.http.post<{ playlist:IPlaylist }>(`list/save/${id}`,params).pipe(
      map(resp => {
        const r = resp.playlist;
        return r;
      }),
      catchError((resp: HttpErrorResponse) => throwError(`Error register
      user. Status: ${resp.status}. Message: ${resp.message}`))
    );
  }

  //get all playlist 
  getAllPlaylist(): Observable<IPlaylist[]>{
    return this.http.get<{playlist:IPlaylist[]}>('list/getall').pipe(
      map(resp =>
        resp.playlist.map(r => {
          r.userCreator.image = `${environment.baseUrl}user/get-image-user/${r.userCreator.image}`;
          r.file =  `${environment.baseUrl}list/get-image-playlist/${r.file}`;
          return r;
        }),
        catchError((resp: HttpErrorResponse) => throwError(`Error register
        user. Status: ${resp.status}. Message: ${resp.message}`))
      )
    );
  }

  //get Me playlist
  getMePlaylist(id:string):Observable<IPlaylist[]>{
    return this.http.get<{ playlist:IPlaylist[] }>(`list/user/${id}`).pipe(
      map(resp => {
        const r = resp.playlist;
        return r;
      })
    );
  }


  deletePlayList(id: string): Observable<void> {
    return this.http.delete<void>(`list/${id}`);
  }

  removeSongPlayList(playlist:IPlaylist,idSong):Observable<boolean>{
    let params  = {
      "songId":idSong
    };
    return this.http.post(`list/deletesong/${playlist._id}`,params).pipe(
      map(resp=>{
        return true;
      })
    );

  }

  postSongToPlayList(playlist:IPlaylist,idSong:string):Observable<any>{
    let params = {
      "songId":idSong
    }
    return this.http.post(`list/addsong/${playlist._id}`,params).pipe(
      map(resp=>{
        const r = resp;
        return r;
      })
    )
  }

  getPlaylist(id:string):Observable<IPlaylist>{
    return this.http.get<{ playlist:IPlaylist }>(`list/detail/${id}`).pipe(
      map(resp => {

        const r = resp.playlist;
        r.userCreator.image = `${environment.baseUrl}user/get-image-user/${r.userCreator.image}`;
        r.file =  `${environment.baseUrl}list/get-image-playlist/${r.file}`;
        return r;
      })
    );
  }

  //redux servicios
  getPlayListRedux(id:string):Observable<any>{
    return this.http.get<{ playlist:IPlaylist }>(`list/detail/${id}`);
  }

  getMePlaylistRedux(id:string):Observable<any>{
    return this.http.get<{playlist:IPlaylist[]}>(`list/user/${id}`);
  }

 

  postSongToPlayListRedux(playlist:IPlaylist,idSong:string):Observable<any>{

    let params = {
      "songId":idSong
    }
    return this.http.post(`list/addsong/${playlist._id}`,params);

  }

}
