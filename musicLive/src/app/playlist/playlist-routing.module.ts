import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistDetailComponent } from './components/playlist-detail/playlist-detail.component';
import { PlayListResolve } from './resolver/play-list-resolve.service';
import { Show } from '../shared/enums/show.enum';
import { UserResolve } from '../user/resolve/user-resolve.service';

export const routes = [
  { 
    path: '',
    redirectTo: 'explore'
  },
  { 
    path: 'explore',
    component: PlaylistListComponent ,
    data: { show: Show.ALL }
  },
  { 
    path: 'detail/:id',
    component: PlaylistDetailComponent,
    pathMatch:'full',
    resolve:{
      playlist:PlayListResolve
    } 
  },
  { 
    path: 'me',
    component: PlaylistListComponent,
    data: { show: Show.MINE },
    resolve:{
      user:UserResolve
    } 
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PlaylistRoutingModule { }
