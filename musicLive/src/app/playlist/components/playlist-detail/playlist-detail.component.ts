import { Component, OnInit } from '@angular/core';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { PlaylistService } from '../../services/playlist.service';
import { ActivatedRoute, Router } from '@angular/router';

import { AppState, selectAuthState } from 'src/app/store/app.state';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { SelectedSong, SelectedPlaylistPersonal } from 'src/app/store/actions/playlist.actions';
import { PlaylistListComponent } from '../playlist-list/playlist-list.component';
import { ISong } from 'src/app/shared/interfaces/song.model';

@Component({
  selector: 'app-playlist-detail',
  templateUrl: './playlist-detail.component.html',
  styleUrls: ['./playlist-detail.component.scss']
})
export class PlaylistDetailComponent implements OnInit {

  image = "https://dummyimage.com/200x200/000/fff";
  playlist:IPlaylist;
  getState: Observable<any>;
  suscription:Subscription;
  mine:boolean;

  constructor(private router: Router,
    private route: ActivatedRoute,
    private playListService:PlaylistService,
    private store: Store<AppState>) {
      this.getState = this.store.select(selectAuthState);
    
    
    }

  ngOnInit() {
    this.playlist = this.route.snapshot.data['playlist'];
 
    this.suscription = this.getState.subscribe((state) => {
      if(state.client){
        let user:any = state.client._id;
        let creator:any = this.playlist.userCreator._id;
        if(user && creator){
          this.mine = user == creator;
        }
      }
 
      
    });

 
    console.log(this.playlist);
  }

  playAlbum() {
    this.store.dispatch(new SelectedPlaylistPersonal(this.playlist));
  }

  playSong(song) {
    this.store.dispatch(new SelectedSong(song));
    console.log(song);
  }

  removeSongFromPlaylist(songDelete:ISong){
    this.playListService.removeSongPlayList(this.playlist,songDelete._id).subscribe((resp)=>{
      if(resp){
        this.playlist.songs = this.playlist.songs.filter(song=>song._id!=songDelete._id);

      }
    })
 
  }

}
