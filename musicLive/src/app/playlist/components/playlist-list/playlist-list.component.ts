import { Component, OnInit } from '@angular/core';
import { PlaylistService } from '../../services/playlist.service';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Show } from 'src/app/shared/enums/show.enum';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { MatDialog, MatSnackBar } from '@angular/material';
import { PlaylistDialogComponent } from 'src/app/shared/dialog/playlist-dialog/playlist-dialog.component';
import { Observable, Subscription } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { AppState, selectedListSong } from 'src/app/store/app.state';
import { UserLoad } from 'src/app/store/actions/auth.actions';
import { PlaylistAddNew, PlaylistRemovedAction } from 'src/app/store/actions/listSong.action';

@Component({
  selector: 'app-playlist-list',
  templateUrl: './playlist-list.component.html',
  styleUrls: ['./playlist-list.component.scss']
})
export class PlaylistListComponent implements OnInit {

  playlist:IPlaylist[];
  mine:boolean;
  getState: Observable<any>;
  suscription:Subscription;

  count$: Observable<IPlaylist[]>;

  constructor(private playlistService:PlaylistService,
     private route: ActivatedRoute,
     public dialog: MatDialog,
     private router: Router,
     private store: Store<AppState>) {
      this.getState = this.store.select(selectedListSong);
  }

  ngOnInit() {

    switch(this.route.snapshot.data.show){
      case Show.ALL:
          this.mine = false;
          this.playlistService.getAllPlaylist().subscribe(resp=>{
            this.playlist = resp;
          });
        break;
      case Show.MINE:
         this.mine = true;
         this.count$ = this.store.pipe(select(s=>s.listSong.MEplaylist));
        break;
    }

  }



   createPlayList(){
    const dialogRef = this.dialog.open(PlaylistDialogComponent, {
      width: '600 px'
    });

    dialogRef.afterClosed().subscribe(result => {
      let newPlayList:IPlaylist = <IPlaylist>result;
      this.store.dispatch(new PlaylistAddNew(newPlayList));
    });
   }

   ngOnDestroy(): void {
     //Called once, before the instance is destroyed.
     //Add 'implements OnDestroy' to the class.
     if(this.suscription)
        this.suscription.unsubscribe();
   }

}
