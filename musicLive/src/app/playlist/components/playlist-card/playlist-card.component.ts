import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAlbum } from 'src/app/shared/interfaces/album.model';
import { Store } from '@ngrx/store';
import { AppState, selectedPlayListState, selectAuthState } from 'src/app/store/app.state';
import { SelectedAlbum } from 'src/app/store/actions/album.actions';
import { Observable, Subscription } from 'rxjs';
import { SelectedPlaylistPersonal } from 'src/app/store/actions/playlist.actions';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { PlaylistService } from '../../services/playlist.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ConfirmationDialogComponent } from 'src/app/shared/components/confirmation-dialog/confirmation-dialog.component';
import { PlaylistRemovedAction } from 'src/app/store/actions/listSong.action';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-playlist-card',
  templateUrl: './playlist-card.component.html',
  styleUrls: ['./playlist-card.component.scss']
})
export class PlaylistCardComponent implements OnInit {


  @Input() playlist: IPlaylist;
  @Input() publiccard:boolean=false;
  getState: Observable<any>;
  suscription:Subscription;
  user:IUser;
  mine:boolean=true;
  @Output() deleted = new EventEmitter<void>();

  constructor(private store: Store<AppState>,
     private playlistService:PlaylistService,
     public snackBar: MatSnackBar,
     public dialog: MatDialog) { 
    this.getState = this.store.select(selectAuthState);
  
  }

  ngOnInit() {
    this.suscription = this.getState.subscribe((state) => {
      if(state.client){
        let user:any = state.client._id;
        let creator:any = this.playlist.userCreator._id;
        if(user && creator){
          this.mine = user == creator;
        }
      }
    });
    
  }

  play(){
    this.store.dispatch(new SelectedPlaylistPersonal(this.playlist));
  }

  delete(){
    this.playlistService.deletePlayList(this.playlist._id).subscribe((resp)=>{
    this.store.dispatch(new PlaylistRemovedAction(this.playlist));
    this.snackBar.open("Playlist borrado correctamente","Cerrar", {panelClass: ['green-snackbar']})
    })
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of this data?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.delete();
      }
    });
  }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.suscription.unsubscribe();
  }

}
