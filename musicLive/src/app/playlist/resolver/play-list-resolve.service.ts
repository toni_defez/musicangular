import { Injectable } from '@angular/core';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { IPlaylist } from 'src/app/shared/interfaces/playlist.model';
import { PlaylistService } from '../services/playlist.service';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PlayListResolve implements Resolve<IPlaylist> {

  constructor(private playlistService:PlaylistService,
    private router: Router) {

     }

     resolve(route: ActivatedRouteSnapshot): Observable<IPlaylist> {
      return this.playlistService.getPlaylist(route.params['id']).pipe(
  
        catchError(error => {
          console.log(error);
          this.router.navigate(['/home']);
          return of(null);
        })
      );
     }


}
