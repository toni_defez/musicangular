import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PlaylistRoutingModule } from './playlist-routing.module';
import { PlaylistListComponent } from './components/playlist-list/playlist-list.component';
import { PlaylistDetailComponent } from './components/playlist-detail/playlist-detail.component';
import { SharedModule } from '../shared/shared.module';
import { PlaylistCardComponent } from './components/playlist-card/playlist-card.component';
import { PlaylistDialogComponent } from '../shared/dialog/playlist-dialog/playlist-dialog.component';

@NgModule({
  declarations: [PlaylistListComponent, PlaylistDetailComponent, PlaylistCardComponent],
  imports: [
    CommonModule,
    PlaylistRoutingModule,
    SharedModule
  ],
  entryComponents: [
    PlaylistDialogComponent
  ],
})
export class PlaylistModule { }
