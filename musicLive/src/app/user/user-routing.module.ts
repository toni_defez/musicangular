import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserPageComponent } from './components/user-page/user-page.component';
import { UserResolve } from './resolve/user-resolve.service';
import { UserFormComponent } from './components/user-form/user-form.component';

const routes: Routes = [
  {
    path: 'me',
    component: UserPageComponent,
    pathMatch: 'full',
    resolve:{
      user:UserResolve
    }
  },
  {
    path: 'edit',
    pathMatch:'full',
    component:UserFormComponent,
    resolve:{
      user:UserResolve
    }
  },
  {
    path: ':id',
    component:UserPageComponent,
    pathMatch:'full',
    resolve:{
      user:UserResolve
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
