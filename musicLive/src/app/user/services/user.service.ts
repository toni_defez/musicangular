import { Injectable } from '@angular/core';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  changeUserProfile(user:IUser):Observable<IUser>{
    return this.http.put<{user:any}>(`user/${user._id}`,user).pipe(
      map(resp => {
        const r = resp.user;
        return <IUser>r;
      })
    );
  }

  getUser(id:string="me"): Observable<IUser> {
    return this.http.get<{ user: IUser }>(`user/me`).pipe(
      map(resp => {
        const r = resp.user;
        let image = r.image
        r.image = `${environment.baseUrl}user/get-image-user/${image}`;
        return r;
      })
    );
  }


  //servicios para redux 
  getUserRedux():Observable<any>{
    return this.http.get<{ user: IUser }>(`user/me`);
  }

}
