import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IUser } from 'src/app/shared/interfaces/user.model';
import { MatSnackBar } from '@angular/material';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit {
  userForm: FormGroup;
  user:IUser;
  error:any;

  constructor(private userService:UserService,
    private router: Router,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    public snackBar: MatSnackBar) { }

  ngOnInit() {

    this.user = this.route.snapshot.data['user'];
    console.log(this.user);
 
    this.userForm = this.fb.group({
        name: [this.user.name,[Validators.required,Validators.minLength(4)]],
        surname: [this.user.surname, Validators.required],
        email:[this.user.email, [Validators.required,Validators.email]]
    });
  }
  get f() { return this.userForm.controls; }

  submit(){
    if(this.userForm.valid){

     
      this.user.email = this.f.email.value;
      this.user.name = this.f.name.value;
      this.user.surname = this.f.surname.value;
      
      //borramos la imagen para que no actualize la ruta 
      delete this.user.image;

      this.userService.changeUserProfile(this.user).subscribe((resp)=>{
        console.log(resp);
        this.snackBar.open("Usuario actualizado correctamente","Cerrar", {panelClass: ['green-snackbar']})
      },
      error=>{
        console.log("Error al hacer el login: ", error);
        this.error=error;
      });
    }
  }

}
