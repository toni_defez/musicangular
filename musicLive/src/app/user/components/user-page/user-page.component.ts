import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute } from '@angular/router';
import { IUser } from 'src/app/shared/interfaces/user.model';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  user :IUser;

  constructor(private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.user = this.route.snapshot.data['user'];
    console.log("Este usuario......");
    console.log(this.user);
  }

  edit(){
    this.router.navigate(['/profile/edit', this.user._id]);
  }

}
