import { TestBed } from '@angular/core/testing';

import { UserResolve } from './user-resolve.service';

describe('UserResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserResolve = TestBed.get(UserResolve);
    expect(service).toBeTruthy();
  });
});
