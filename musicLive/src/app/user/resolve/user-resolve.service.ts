import { Injectable } from '@angular/core';
import { UserService } from '../services/user.service';
import { Router, ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { IUser } from 'src/app/shared/interfaces/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserResolve implements Resolve<IUser>{

  constructor(private userService: UserService, private router: Router) {}
  
  resolve(route: ActivatedRouteSnapshot): Observable<IUser> {
 
      return this.userService.getUser(route.params['id']).pipe(
        catchError(error => {
          console.log(error);
          this.router.navigate(['/home']);
          return of(null);
        })
      );
  }
}
